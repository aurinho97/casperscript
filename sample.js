var fs = require('fs'),
    noImg = fs.open('results/images.txt', 'w'),
    noOptions = fs.open('results/options.txt', 'w'),
    res404 = fs.open('results/404.txt', 'w'),
    noPrice = fs.open('results/prices.txt', 'w'),
    listItems = [],
    total = 0,
    productId = 1,
    entity,
    progress = 0,
    old,
    sku,
    log = {
        reason: '',
        link: '',
        sku: '',
        page: ''
    },
    results = {
        options: 0,
        images: 0,
        res404: 0,
        prices: 0
    };

phantom.addCookie({
    'name': 'testme',
    'value': 'testme',
    'domain': 'httpbin.org',
    'path': '/'
});

phantom.cookiesEnabled = false;

var casper = require('casper').create();

casper.start('https://www.intersport.no/sykkel-1', function() {
    if (this.exists('.product_count_total')) {
        entity = parseInt(this.getHTML('.product_count_total'));
        total += this.evaluate(function () {
            var subTotal = parseInt(document.querySelector('.product_count_total').innerHTML);
            return Math.ceil(subTotal / 20);
        });
        this.echo('Total pages: ' + total);
        this.echo('Total products: ' + entity);
    } else {
        this.echo('Cannot found number of pages, try to restart');
        casper.exit();
    }
});
casper.then(function () {
    var i = 1;
    function loop(times) {
        var z = 0;
        casper.thenOpen('https://www.intersport.no/sykkel-1' + '?p=' + i + '', function() {
            log.page = i;
            listItems = this.evaluate(function () {
                var items = document.querySelectorAll('.product.photo.product-item-photo');
                return [].map.call(items, function (item) {
                    return item.href;
                });
            });
        }).then(function () {
            function secondLoop() {
                casper.thenOpen(listItems[z], function () {
                    console.log(listItems[z]);
                    if (this.exists('#product_addtocart_form')) {
                        var element = casper.getElementInfo('#product_addtocart_form');
                        sku = element.attributes["data-product-sku"];
                    }
                    progress = Math.round(productId * 100 / entity);
                    if (progress !== old) {
                        console.log('Working... [' + progress + '%]');
                        old = progress;
                    }
                }).then(function () {
                    //-------Searching products without images-------//
                    if (this.exists('.MagicZoom') && !this.exists('.bluefoot-entity.bluefoot-entity.bluefoot-align-center')) {
                        var info = this.getElementInfo('.MagicZoom');
                        var infoHref = info.attributes.href;
                        if (infoHref === 'https://www.intersport.no/media/catalog/product/placeholder/default/no-image-797x797.jpg') {
                            log.reason = 'No image';
                            log.link = listItems[z];
                            log.sku = sku;
                            results.images++;
                            noImg.writeLine(JSON.stringify(log));
                            noImg.close();
                            noImg = fs.open('results/images.txt', 'a');
                        }
                    } else if (!this.exists('.bluefoot-entity.bluefoot-entity.bluefoot-align-center')) {
                        var a = 0;
                        function loopForWait() {
                            casper.reload(function () {
                            }).then(function () {
                                casper.waitForSelector('.MagicZoom',
                                    function success() {
                                    },
                                    function failed() {
                                        a++;
                                        if (a < 3) {
                                            loopForWait();
                                        } else {
                                            log.reason = 'No image';
                                            log.link = listItems[z];
                                            log.sku = sku;
                                            results.images++;
                                            noImg.writeLine(JSON.stringify(log));
                                            noImg.close();
                                            noImg = fs.open('results/images.txt', 'a');
                                        }
                                    },
                                    5000
                                );
                            });
                        }
                        loopForWait();
                    }
                }).then(function () {
                    //-------Searching products without price--------//
                    if (!this.exists('.price-wrapper .price') && !this.exists('.bluefoot-entity.bluefoot-entity.bluefoot-align-center')) {
                        log.reason = 'No price';
                        log.link = listItems[z];
                        log.sku = sku;
                        results.prices++;
                        noPrice.writeLine(JSON.stringify(log));
                        noPrice.close();
                        noPrice = fs.open('results/prices.txt', 'a');
                    }
                    if (this.exists('.price-wrapper .price')) {
                        var price = this.getElementInfo('.price-wrapper .price').html;
                        var nPrice = price.split('');
                        if (nPrice.length < 6) {
                        } else if (nPrice.length < 13) {
                            nPrice.splice(1, 6);
                        } else {
                            nPrice.splice(2, 7);
                        }
                        nPrice.pop();
                        nPrice.pop();
                        nPrice = nPrice.join('');
                        if (parseInt(nPrice) < 1) {
                            log.reason = 'Price = 0';
                            log.link = listItems[z];
                            log.sku = sku;
                            results.prices++;
                            noPrice.writeLine(JSON.stringify(log));
                            noPrice.close();
                            noPrice = fs.open('results/prices.txt', 'a');
                        }
                    }
                }).then(function() {
                    //--------Searching products with 404-------------//
                    if (this.exists('.bluefoot-entity.bluefoot-entity.bluefoot-align-center')) {
                        log.reason = '404';
                        log.link = listItems[z];
                        log.sku = sku;
                        results.res404++;
                        res404.writeLine(JSON.stringify(log));
                        res404.close();
                        res404 = fs.open('results/404.txt', 'a');
                    }
                }).then(function () {
                    //-------Searching products without attributes controller-----//
                    if (!this.exists('.product-options-wrapper') && !this.exists('.bluefoot-entity.bluefoot-entity.bluefoot-align-center')) {
                        log.reason = 'No options';
                        log.link = listItems[z];
                        log.sku = sku;
                        results.options++;
                        noOptions.writeLine(JSON.stringify(log));
                        noOptions.close();
                        noOptions = fs.open('results/options.txt', 'a');
                    }
                }).then(function () {
                    z++;
                    productId++;
                    if (z < listItems.length) {
                        secondLoop();
                    } else {
                        i++;
                        if (i <= times) {
                            loop(total);
                        } else {
                            noImg.close();
                            res404.close();
                            noOptions.close();
                            noPrice.close();
                        }
                    }
                });
            }
            secondLoop();
        });
    }
    loop(total);
});
casper.then(function () {
    this.echo('Results:\n   Products without images: '+ results.images +'\n   Products without prices: '+ results.prices +'\n   Products without attributes options: '+ results.options +'\n   Products with 404: '+ results.res404 +'');
});
casper.run();